// NginxGUIDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NginxGUI.h"
#include "NginxGUIDlg.h"
#include "StringUtils.h"
#include "ProcessUtils.h"
#include "Service.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNginxGUIDlg dialog

CNginxGUIDlg::CNginxGUIDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNginxGUIDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNginxGUIDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	theApp = (CNginxGUIApp*)AfxGetApp();
}

void CNginxGUIDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNginxGUIDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CNginxGUIDlg, CDialog)
	//{{AFX_MSG_MAP(CNginxGUIDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, OnStartNginx)
	ON_BN_CLICKED(IDC_BUTTON2, OnStopNginx)
	ON_BN_CLICKED(IDC_BUTTON3, OnEditConf)
	ON_BN_CLICKED(IDC_BUTTON4, OnReloadConf)
	ON_BN_CLICKED(IDC_BUTTON5, OnOpenLogDir)
	ON_BN_CLICKED(IDC_BUTTON6, OnViewVersion)
	ON_BN_CLICKED(IDC_BUTTON7, OnReOpenLog)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK1, OnSwitchAutoStart)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNginxGUIDlg message handlers

BOOL CNginxGUIDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	// 修改当前窗体标题
	CString strDlgTitle;
	GetWindowText(strDlgTitle);
	SetWindowText(strDlgTitle + " - " + theApp->m_strAppBaseDir);

	// 启动定时器
	::SetTimer(this->m_hWnd, 1, 3000, NULL);

	// 刷新进程
	RefreshProcess();

	// 更新复选框状态
	UpdateCheckStatus();
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CNginxGUIDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CNginxGUIDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CNginxGUIDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

// 启动Nginx
void CNginxGUIDlg::OnStartNginx() 
{
	theApp->StartNginx();
	RefreshProcess();
}

// 停止Nginx
void CNginxGUIDlg::OnStopNginx() 
{
	if (!IsNginxRun(true))
		return;

	theApp->StopNginx();
	RefreshProcess();
}

// 编辑配置
void CNginxGUIDlg::OnEditConf() 
{
	theApp->RunCommand("open",theApp->m_strNginxConf,"","",1);	
}

// 重载配置
void CNginxGUIDlg::OnReloadConf() 
{
	if (!IsNginxRun(true))
		return;

	theApp->RunCommand("open", theApp->m_strNginxExePath, "-s reload", theApp->m_strAppBaseDir, 0);
}

// 打开日志目录
void CNginxGUIDlg::OnOpenLogDir() 
{
	theApp->RunCommand("open", theApp->m_strNginxLogDir, "", "", 1);	
}

// 查看版本
void CNginxGUIDlg::OnViewVersion() 
{
	CString strOutFile =  theApp->m_strNginxLogDir + "\\version.log";
	CString strParams = CStringUtils::FormatString("cmd.exe /C \"%s -V 2>%s\"", 
		theApp->m_strNginxExePath, strOutFile);
	
	theApp->RunCommand("open", "cmd.exe", strParams, theApp->m_strAppBaseDir, 0);
	Sleep(300); // 休眠一下，防止打开文件为空
	theApp->RunCommand("open", strOutFile, "", "", 1);
}


// 刷新进程信息
void CNginxGUIDlg::RefreshProcess(){
	PSIMPROCESS process;
	process = (PSIMPROCESS)malloc(sizeof(SIMPROCESS) * MAX_PROCESS);
	int num = CProcessUtils::GetProcess("nginx.exe", theApp->m_strNginxExePath, process, 0);
	CString info="";
	for(int i=0;i<num;i++){
		CString line;
		line.Format("%s  %s\r\n", process[i].szName, process[i].szPath);
		info += line;
	}
	CEdit *editor = (CEdit *)GetDlgItem(IDC_EDIT1);
	editor->SetWindowText(info);
	
	free(process);
	process = NULL;
}

void CNginxGUIDlg::OnTimer(UINT nIDEvent) 
{
	RefreshProcess();	
	CDialog::OnTimer(nIDEvent);
}


// 更新复选框状态
void CNginxGUIDlg::UpdateCheckStatus()
{
	CButton * button = (CButton *)GetDlgItem(IDC_CHECK1);
	button->SetCheck(_Module.IsInstalled());
}

// 开启启动切换
void CNginxGUIDlg::OnSwitchAutoStart() 
{
	CButton *button = (CButton*)GetDlgItem(IDC_CHECK1);
	int nCheck = button->GetCheck();

	if(nCheck == 0)
		_Module.Uninstall();
	else
		_Module.Install();

	// ShowError();

	UpdateCheckStatus();
}


void CNginxGUIDlg::OnReOpenLog()
{
	if (!IsNginxRun(true))
		return;

	// 先重命名日志文件，包括 access.log 和 error.log
	// 为了兼容多个站点的情况，access.log, access1.log, access2.log, ... 都会被重命名
	// error.log 同样

	CTime tm;
	tm = CTime::GetCurrentTime();
	CString sTime = tm.Format("%Y%m%d_%H%M%S");

	for (int i = 0; i < 10; ++i) {
		CString sNo;

		if (i == 0)
			sNo = "";
		else
			sNo.Format("%d", i + 1);

		CString sFiles[2] = {"access", "error"};
		for (int j = 0; j < 2; ++j) {
			CString sFile, sSrcFile, sDstFile;
			sFile = sFiles[j];
			sSrcFile.Format("%s\\%s%s.log", theApp->m_strNginxLogDir, sFile, sNo);
			sDstFile.Format("%s\\%s%s.backup.%s.log", theApp->m_strNginxLogDir, sFile, sNo, sTime);
			if (-1 != GetFileAttributes(sSrcFile)) {
				rename(sSrcFile, sDstFile);
			}
		}
	}

	Sleep(300);

	theApp->RunCommand("open", theApp->m_strNginxExePath, "-s reopen", theApp->m_strAppBaseDir, 0);
}


bool CNginxGUIDlg::IsNginxRun(BOOLEAN bShowAlert)
{
	CEdit* editor = (CEdit*)GetDlgItem(IDC_EDIT1);
	CString sInfo;
	editor->GetWindowText(sInfo);

	if (sInfo.GetLength() == 0) {
		if (bShowAlert) {
			MessageBox(_T("Nginx 貌似没有启动！"), _T("操作无效"), MB_OK | MB_ICONERROR);
		}
		return false;
	}

	return true;
}
