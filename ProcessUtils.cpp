// ProcessUtils.cpp: implementation of the CProcessUtils class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NginxGUI.h"
#include "ProcessUtils.h"
#include "StringUtils.h"
#include <tlhelp32.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CProcessUtils::CProcessUtils()
{
	
}

CProcessUtils::~CProcessUtils()
{

}

//获取进程命令行
BOOL CProcessUtils::GetProcessCommandLine(HANDLE hProcess, LPTSTR pszCmdLine, DWORD cchCmdLine)
{
	BOOL			bRet;
	DWORD			dwPos;
	LPBYTE			lpAddr;
	DWORD			dwRetLen;
 
	bRet = FALSE;
 
	dwPos = 0;
	lpAddr = (LPBYTE)GetCommandLine;
Win7:
	if(lpAddr[dwPos] == 0xeb && lpAddr[dwPos + 1] == 0x05)
	{
		dwPos += 2;
		dwPos += 5;
Win8:
		if(lpAddr[dwPos] == 0xff && lpAddr[dwPos + 1] == 0x25)
		{
			dwPos += 2;
			lpAddr = *(LPBYTE*)(lpAddr + dwPos);
 
			dwPos = 0;
			lpAddr = *(LPBYTE*)lpAddr;
WinXp:
			if(lpAddr[dwPos] == 0xa1)
			{
				dwPos += 1;
				lpAddr = *(LPBYTE*)(lpAddr + dwPos);
				bRet = ReadProcessMemory(hProcess,
					lpAddr,
					&lpAddr,
					sizeof(LPBYTE),
					&dwRetLen);
				if(bRet)
				{
					bRet = ReadProcessMemory(hProcess,
						lpAddr,
						pszCmdLine,
						cchCmdLine,
						&dwRetLen);
				}
			}
		}
		else
		{			
			goto WinXp;
		}
	}
	else
	{
		goto Win8;
	}
 
	return bRet;
}

// 根据进程名和进程路径，杀死进程
void CProcessUtils::KillByNameAndPath(CString strName, CString strPath)
{
	SIMPROCESS *process;

	process=(SIMPROCESS *)malloc(sizeof(SIMPROCESS) * MAX_PROCESS);

	GetProcess(strName,strPath,process, 9);

	free(process);
	process=NULL;
}

// 查询进程
int CProcessUtils::GetProcess(CString strName, CString strPath, SIMPROCESS *process, int oper){
	PROCESSENTRY32	pe32;
	TCHAR			szImagePath[MAX_PATH];
	HANDLE			hProcess = NULL;

    pe32.dwSize=sizeof(pe32);
    HANDLE hProcessSnap=CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);
    if(hProcessSnap==INVALID_HANDLE_VALUE)
    {
       return 0;
    }
    
    BOOL bMore=Process32First(hProcessSnap,&pe32);
	int i=0;
	
    while(bMore)
    {
        // pe32.szExeFile,pe32.th32ProcessID
		if(strName == pe32.szExeFile){
			hProcess = OpenProcess(PROCESS_TERMINATE|PROCESS_VM_READ,FALSE,pe32.th32ProcessID);
			if(hProcess){
				if(GetProcessCommandLine(hProcess, szImagePath, MAX_PATH))
				{
					CString strImagePath = CString(szImagePath);
					CString cs1 = CStringUtils::FormatPath(strImagePath);
					CString cs2 = CStringUtils::FormatPath(strPath);

					if(cs1.Find(cs2) != -1){
						process[i].nProcessId = pe32.th32ProcessID;
						strcpy(process[i].szName, pe32.szExeFile);
						strcpy(process[i].szPath, strImagePath);

						if(oper == 9)
							TerminateProcess(hProcess, 0);

						i++;
					}
				}
			}
		}
        bMore=Process32Next(hProcessSnap,&pe32);
    }
	
	CloseHandle(hProcess);
    CloseHandle(hProcessSnap);

	return i;
}

BOOL CProcessUtils::EnableDebugPrivilege()
{
  HANDLE hToken;
  BOOL fOk=FALSE;
  if(OpenProcessToken(GetCurrentProcess(),TOKEN_ADJUST_PRIVILEGES,&hToken))
  {
    TOKEN_PRIVILEGES tp;
    tp.PrivilegeCount=1;
    if(!LookupPrivilegeValue(NULL,SE_DEBUG_NAME,&tp.Privileges[0].Luid))
      printf("Can't lookup privilege value.\n");
    tp.Privileges[0].Attributes=SE_PRIVILEGE_ENABLED;
    if(!AdjustTokenPrivileges(hToken,FALSE,&tp,sizeof(tp),NULL,NULL))
      printf("Can't adjust privilege value.\n");
    fOk=(GetLastError()==ERROR_SUCCESS);
    CloseHandle(hToken);
  }
    return fOk;
}