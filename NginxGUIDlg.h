// NginxGUIDlg.h : header file
//

#if !defined(AFX_NGINXGUIDLG_H__7144B9FF_9C02_4AC0_850F_290095D7F5D0__INCLUDED_)
#define AFX_NGINXGUIDLG_H__7144B9FF_9C02_4AC0_850F_290095D7F5D0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CNginxGUIDlg dialog

class CNginxGUIDlg : public CDialog
{
// Construction
public:
	CNginxGUIDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CNginxGUIDlg)
	enum { IDD = IDD_NGINXGUI_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNginxGUIDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	CNginxGUIApp *theApp;

	// Generated message map functions
	//{{AFX_MSG(CNginxGUIDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnStartNginx();
	afx_msg void OnStopNginx();
	afx_msg void OnEditConf();
	afx_msg void OnReloadConf();
	afx_msg void OnOpenLogDir();
	afx_msg void OnReOpenLog();
	afx_msg void OnViewVersion();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnSwitchAutoStart();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	// my private functions
	void RefreshProcess();
	void UpdateCheckStatus();	
public:
	bool IsNginxRun(BOOLEAN bShowAlert);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NGINXGUIDLG_H__7144B9FF_9C02_4AC0_850F_290095D7F5D0__INCLUDED_)
