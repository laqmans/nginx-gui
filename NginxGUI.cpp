// NginxGUI.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "NginxGUI.h"
#include "NginxGUIDlg.h"
#include "Service.h"
#include "ProcessUtils.h"
#include "MD5.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNginxGUIApp

BEGIN_MESSAGE_MAP(CNginxGUIApp, CWinApp)
	//{{AFX_MSG_MAP(CNginxGUIApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNginxGUIApp construction

CNginxGUIApp::CNginxGUIApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CNginxGUIApp object

CNginxGUIApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CNginxGUIApp initialization

BOOL CNginxGUIApp::InitInstance()
{

	// 提权
	CProcessUtils::EnableDebugPrivilege();

	// 获取当前程序目录
	GetAppBaseDir();

	// 获取Nginx镜像路径的MD5码
	MD5 md5;
	md5.update((LPCTSTR)m_strNginxExePath);
	string sPathMD5 = md5.toString().substr(0,6);
	md5.reset();

	// 初始化服务实例
	CString strSrvName;
	strSrvName.Format("NginxAutoStart_%s", sPathMD5.c_str());
	
	_Module.Init(strSrvName,strSrvName);

	CString strCMD = CString(this->m_lpCmdLine);

	// 来自服务的调用
	if(strCMD.Find("/RunAsService") >=0){
		_Module.m_bService = TRUE;
		_Module.Start();
		return false;
	}
	
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	CNginxGUIDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}


// 获取程序运行目录
void CNginxGUIApp::GetAppBaseDir(){
	GetModuleFileName(NULL,m_strAppBaseDir.GetBuffer(MAX_PATH),MAX_PATH);

	m_strAppBaseDir.ReleaseBuffer();

	int pos= m_strAppBaseDir.ReverseFind(_T('\\'));

	m_strAppBaseDir = m_strAppBaseDir.Left(pos);
	m_strNginxExePath = m_strAppBaseDir + "\\nginx.exe";
	m_strNginxLogDir = m_strAppBaseDir + "\\logs";
	m_strNginxConf = m_strAppBaseDir + "\\conf\\nginx.conf";
}

void CNginxGUIApp::StartNginx(){
	StopNginx();
	RunCommand("open", m_strNginxExePath, "", m_strAppBaseDir, 0);
}

void CNginxGUIApp::StopNginx(){
	CProcessUtils::KillByNameAndPath("nginx.exe", m_strNginxExePath);
}


// 执行命令
void CNginxGUIApp::RunCommand(LPCTSTR lpOper, LPCTSTR lpFile, LPCTSTR lpParams, LPCTSTR lpDir,INT nShowCmd){
	ShellExecute(NULL,"open", lpFile, lpParams, lpDir,nShowCmd);
	ShowError();
}


// 显示错误信息
void CNginxGUIApp::ShowError(){
	DWORD nErrCode = GetLastError();

	if(nErrCode == 0)
		return;

	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		nErrCode,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
		(LPTSTR) &lpMsgBuf,
		0,
		NULL
	);

	MessageBox(NULL, (LPCTSTR)lpMsgBuf, _T("发生错误"), MB_OK | MB_ICONERROR);
    LocalFree( lpMsgBuf );
}