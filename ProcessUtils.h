// ProcessUtils.h: interface for the CProcessUtils class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROCESSUTILS_H__EDD621FE_809A_4D5E_9AE8_007D62B41A1D__INCLUDED_)
#define AFX_PROCESSUTILS_H__EDD621FE_809A_4D5E_9AE8_007D62B41A1D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MAX_PROCESS 16

typedef struct tagSIMPROCESS {
	DWORD nProcessId;		// 进程ID
	CHAR szName[MAX_PATH];  // 镜像名
	CHAR szPath[MAX_PATH];  // 完整路径
} SIMPROCESS;
typedef SIMPROCESS * PSIMPROCESS;

// typedef SIMPROCESSES * SIMPROCESS;

class CProcessUtils  
{
public:
	static BOOL GetProcessCommandLine(HANDLE hProcess, LPTSTR pszCmdLine, DWORD cchCmdLine);
	static void KillByNameAndPath(CString strName, CString strPath);
	static int GetProcess(CString strName, CString strPath, SIMPROCESS *process, int oper);
	static BOOL EnableDebugPrivilege();

	CProcessUtils();
	virtual ~CProcessUtils();

};

#endif // !defined(AFX_PROCESSUTILS_H__EDD621FE_809A_4D5E_9AE8_007D62B41A1D__INCLUDED_)
