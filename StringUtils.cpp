// StringUtils.cpp: implementation of the CStringUtils class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NginxGUI.h"
#include "StringUtils.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CStringUtils::CStringUtils()
{

}

CStringUtils::~CStringUtils()
{

}

// 格式化字符串
CString CStringUtils::FormatString(LPCTSTR pstrFormat, ...)
{
	CString strResult;
 
	va_list args;
	va_start(args, pstrFormat);
	strResult.FormatV(pstrFormat, args);
	va_end(args);
 
	return strResult;
}

// 格式化路径
CString CStringUtils::FormatPath(CString path){
	path.Replace("\\", "/");
	path.MakeLower();
	return path;
}