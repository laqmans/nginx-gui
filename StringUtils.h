// StringUtils.h: interface for the CStringUtils class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STRINGUTILS_H__B3E6E83B_CF26_46DB_8CFE_E27B2ADEEC7D__INCLUDED_)
#define AFX_STRINGUTILS_H__B3E6E83B_CF26_46DB_8CFE_E27B2ADEEC7D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CStringUtils  
{
public:
	static CString FormatString(LPCTSTR pstrFormat, ...);
	static CString FormatPath(CString path);
	CStringUtils();
	virtual ~CStringUtils();

};

#endif // !defined(AFX_STRINGUTILS_H__B3E6E83B_CF26_46DB_8CFE_E27B2ADEEC7D__INCLUDED_)
