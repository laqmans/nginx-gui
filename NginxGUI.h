// NginxGUI.h : main header file for the NGINXGUI application
//

#if !defined(AFX_NGINXGUI_H__33242F0F_CDB1_43D9_AE25_1D487486F355__INCLUDED_)
#define AFX_NGINXGUI_H__33242F0F_CDB1_43D9_AE25_1D487486F355__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CNginxGUIApp:
// See NginxGUI.cpp for the implementation of this class
//

class CNginxGUIApp : public CWinApp
{
public:
	CNginxGUIApp();

	// my memebers
	CString m_strAppBaseDir;
	CString m_strNginxExePath;
	CString m_strNginxConf;
	CString m_strNginxLogDir;

	// my public functions
	void RunCommand(LPCTSTR lpOper, LPCTSTR lpFile, LPCTSTR lpParams, LPCTSTR lpDir,INT nShowCmd);
	void StartNginx();
	void StopNginx();
	void ShowError();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNginxGUIApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CNginxGUIApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	private:
		void GetAppBaseDir();
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NGINXGUI_H__33242F0F_CDB1_43D9_AE25_1D487486F355__INCLUDED_)
